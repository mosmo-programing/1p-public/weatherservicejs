// Define elments
const map = document.querySelector('.map')

// Load subSite
const contentMap = document.querySelector(`.content-map`)

loadSubMap = (name) => {
    if (name == "close") {
        contentMap.classList.add("hidden-content-map")
    }
    else {
        // ładowanie treści dostarczonych przez jagodę
        const elementI = document.querySelector(`.map-${name}`)
        const title = document.querySelector(`.content-map-title`)
        const photo = document.querySelector(`.photo-content-map-inside`)
        
        const infoContentI = document.querySelector(`.info-content-i`)
        const wojewodztwo = document.querySelector(`.wojewodztwo`)
        const populacja = document.querySelector(`.populacja`)
        const lokacje = document.querySelector(`.lokacje`)

        wojewodztwo.innerHTML = "Województwo: " + dane[name]["wojewodztwo"]
        populacja.innerHTML = "W " + citysNames[name] + " mieszka: " + dane[name]["populacja"]
        lokacje.innerHTML = "Popularne lokacje w " + citysNames[name] + ": " + dane[name]["lokacje"]

        title.innerHTML = citysNames[name]
        photo.style.width = "50vw"
        photo.style.height = "30vh"
        photo.style.backgroundSize = "cover"
        photo.style.backgroundImage = "url('./img/places/" + name + ".jpg')"

        contentMap.classList.remove("hidden-content-map")
    }
}

// Render elements
let day = days_oftheweek[now.getDay()]
map.innerHTML += `<div id='map-${day}'><div id='photo'></div></div>`

const sub_map = document.querySelector(`#map-${day}`)
citys.forEach(e => { 
    sub_map.innerHTML += `<div class='map-${e} sub-map' onclick='loadSubMap("${e}")' style=''></div>` 

    const element = document.querySelector(`.map-${e}`)
    element.style.position = "relative"
    element.style.backgroundImage = "url('./img/maps/zz/city/" + e + ".png')"
    element.style.width = style[e][0] + "px"
    element.style.height = style[e][1] + "px"
    element.style.top = style[e][2] + "px"
    element.style.left = style[e][3] + "px"
    element.style.backgroundSize = "cover";
    element.style.color = "transparent"   
    element.style.cursor = "pointer"
})
// Tak