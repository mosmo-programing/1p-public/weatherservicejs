// Skrypt odpowiadajcy za uzupełnienie .content i pod elementów w index.html

let actCityQ = document.querySelector('.actual-city')
let actCityList = [
    "jg", "wr", "kt", "kr", "wwa"
]
let actCityListDes = {
    "jg": "Jelenia Góra",
    "wr": "Wrocław",
    "kt": "Katowice",
    "kr": "Kraków",
    "wwa": "Warszawa"
}

//Klasy Ogólne
let content = document.querySelector(".content")
let content_data = document.querySelector(".content_data")
let content_block = document.querySelector(".content_block")

//Klasy - Pojemniki
let content_title = document.querySelector(".content_title")
let content_map = document.querySelector(".content_map")
let content_data_temp = document.querySelector(".content_temp")
let content_data_status = document.querySelector(".content_status")

load_city = () => {
    actCityQ.innerHTML = actCityListDes[getCookie("city")]

    document.getElementById(getCookie("city")).classList.add("hide")
    actCityList.forEach(e => {
        if (e != getCookie("city")){
            document.getElementById(e).classList.remove("hide")
        }
    })
}
render = () => {
    let d = new Date()
    actCityList.forEach(e => {
        if (e == getCookie("city")){
            let today
            if (d.getDay() == 0) today = "Niedz"
            if (d.getDay() == 1) today = "Pon"
            if (d.getDay() == 2) today = "Wt"
            if (d.getDay() == 3) today = "Sro"
            if (d.getDay() == 4) today = "Czw"
            if (d.getDay() == 5) today = "Pt"
            if (d.getDay() == 6) today = "Sob"


            let img = index[today][e].img
            let temp = index[today][e].temp[d.getHours()]
            let wl = index[today][e].wl[d.getHours()]
            let st = index[today][e].stan


            content_title.innerHTML = "<p class='content-title-p'>Pogoda dla: " + actCityListDes[e] + "</p>"
            content_map.innerHTML = "<img src='" + img + "' class='content-img'></img"
            content_data_temp.innerHTML = "<p class='content-data-temp-p'>" + temp + " stopni celciusza</p>"
            content_data_status.innerHTML = 
            "<p class='content-data-status-st-p' >Aktualny stan pogodowy: " + st + "</p>" +
            "<p class='content-data-status-wl-p' >" + wl + "% wilgotności powietrza</p>" 
            
        }
    })
}
async function reload (selectedCity) {
    setCookie(selectedCity, "city")

    await load_city()
    await render()
}

// Init
update = () => {
    load_days()
    load_hour()
    setTimeout(update, 490)
}
updateContent = () => {
    render()
    setTimeout(updateContent, 100000)
}

load_city()
update()
updateContent()