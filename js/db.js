//Symulacja Bazy Danych

// 
//Wymagana zmiana danych, dane są ustawione na takie same tylko na czas testów
// 

let index = {
    "Pon": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [8,8,9,10,10,10,11,11,12,11,13,14,12,11,11,11,12,10,9,9,8,7,8,7],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [11,11,12,10,13,13,12,12,14,13,13,15,13,12,12,10,10,11,10,10,9,9,9,8],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [8,8,9,10,10,10,11,11,12,11,13,14,12,11,11,11,12,10,9,9,8,7,8,7],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [7,7,8,9,9,9,10,10,11,10,12,13,11,10,10,10,11,9,8,8,7,6,7,6],
            "wl": [64,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [9,9,10,11,11,11,12,12,13,12,14,15,13,12,12,12,13,11,10,10,9,8,9,8],
            "wl": [63,64,65,64,65,66,63,63,64,70,67,68,66,65,67,67,71,75,77,78,80,79,77,76],
            "stan": "Zachmurzenie"
        }
    },
    "Wt": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [8,8,8,9,9,9,10,10,11,12,13,14,15,16,16,17,16,16,15,14,13,12,11,10],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Słonecznie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [8,9,9,9,8,9,10,10,10,11,12,14,16,17,18,16,15,15,13,12,11,11,10,9],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [14,14,14,13,13,12,12,11,11,12,13,14,15,15,16,15,14,13,12,11,10,10,10,10],
            "wl": [63,64,65,64,65,66,63,63,64,70,67,68,66,65,67,67,71,75,77,78,80,79,77,76],
            "stan": "Słonecznie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [8,8,9,10,10,10,11,11,12,11,13,14,12,11,11,11,12,10,9,9,8,7,8,7],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Niewielkie Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [11,11,12,10,13,13,12,12,14,13,13,15,13,12,12,10,10,11,10,10,9,9,9,8],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Słonecznie"
        }
    },
    "Sro": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [9,9,10,10,10,11,12,13,12,11,12,13,14,15,15,15,14,13,12,12,11,11,11,11],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [10,11,11,11,11,11,12,13,14,14,15,15,16,15,17,15,14,14,13,12,10,10,9,9],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [11,12,12,12,12,12,13,14,15,15,16,16,17,16,18,16,15,15,14,13,11,11,10,10],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [6,6,5,6,7,8,9,9,9,10,10,11,11,12,13,14,14,13,13,12,11,11,10,9],
            "wl": [59,61,62,61,62,64,60,60,61,67,64,65,63,62,64,64,68,72,74,75,77,76,74,74],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [6,6,5,6,7,8,9,9,9,10,10,11,11,12,13,14,14,13,13,12,11,11,10,9],
            "wl": [63,64,65,64,65,66,63,63,64,70,67,68,66,65,67,67,71,75,77,78,80,79,77,76],
            "stan": "Zachmurzenie"
        }
    },
    "Czw": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [11,11,11,11,11,11,11,10,10,12,14,16,17,17,18,17,17,16,15,14,14,13,13,12],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [10,9,9,9,9,9,9,8,9,10,12,14,16,17,17,17,16,14,13,13,13,13,12,12],
            "wl": [57,61,62,61,62,64,60,60,61,67,64,65,63,62,64,64,68,72,74,75,77,74,77,72],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [11,11,12,10,13,13,12,12,14,13,13,15,13,12,12,10,10,11,10,10,9,9,9,8],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [9,9,10,11,11,11,12,12,13,12,14,15,13,12,12,12,13,11,10,10,9,8,9,8],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [8,8,9,10,10,10,11,11,12,11,13,14,12,11,11,11,12,10,9,9,8,7,8,7],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        }
    },
    "Pt": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [12,12,12,12,12,12,12,11,12,13,14,15,15,16,16,16,15,14,13,13,13,13,12,12],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [14,14,14,13,13,12,12,11,11,12,13,14,15,15,16,15,14,13,12,11,10,10,10,10],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [10,9,9,9,9,9,9,8,9,10,12,14,16,17,17,17,16,14,13,13,13,13,12,12],
            "wl": [59,61,62,61,62,64,60,60,61,67,64,65,63,62,64,64,68,72,74,75,77,76,74,74],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [12,12,12,12,12,12,12,11,12,13,14,16,15,16,16,14,15,14,13,13,13,13,13,12],
            "wl": [63,64,65,64,65,66,63,63,64,70,67,68,66,65,67,67,71,75,77,78,80,79,77,76],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [11,11,12,10,13,13,12,12,14,13,13,15,13,12,12,10,10,11,10,10,9,9,9,8],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        }
    },
    "Sob": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [12,12,12,11,11,10,10,9,9,10,11,12,13,13,14,13,12,11,10,9,8,8,8,8],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [13,13,13,12,12,11,11,10,10,11,12,13,14,14,15,14,13,12,11,10,9,9,9,9],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [11,11,11,10,10,9,9,8,8,9,10,11,12,12,13,12,11,10,9,8,7,7,7,7],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [14,14,14,13,13,12,12,11,11,12,13,14,15,15,16,15,14,13,12,11,10,10,10,10],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [10,10,10,9,9,8,8,7,7,8,9,10,11,11,12,11,10,9,8,7,8,8,8,8],
            "wl": [59,61,62,61,62,64,60,60,61,67,64,65,63,62,64,64,68,72,74,75,77,76,74,74],
            "stan": "Zachmurzenie"
        }
    },
    "Niedz": {
        "jg": {
            "img": "img/maps/s_g/jeleniagora.png",
            "temp": [8,7,7,7,7,7,7,6,7,8,10,12,14,15,15,15,14,12,11,11,11,11,10,10],
            "wl": [63,64,65,64,65,66,63,63,64,70,67,68,66,65,67,67,71,75,77,78,80,79,77,76],
            "stan": "Zachmurzenie"
        },
        "wr": {
            "img": "img/maps/s_g/wroclaw.png",
            "temp": [9,8,8,8,8,8,8,7,8,9,11,13,15,16,16,16,15,13,12,12,12,12,11,11],
            "wl": [60,62,63,62,63,65,61,61,62,68,65,66,64,63,65,65,69,73,75,76,78,77,75,74],
            "stan": "Zachmurzenie"
        },
        "kt": {
            "img": "img/maps/s_g/katowice.png",
            "temp": [7,6,6,6,6,6,6,5,6,7,9,11,13,14,14,14,13,11,10,10,10,10,9,9],
            "wl": [75,77,78,80,79,79,78,74,70,66,65,64,63,67,71,75,76,76,77,78,79,80,77,74],
            "stan": "Zachmurzenie"
        },
        "kr": {
            "img": "img/maps/s_g/krakow.png",
            "temp": [10,9,9,9,9,9,9,8,9,10,12,14,16,17,17,17,16,14,13,13,13,13,12,12],
            "wl": [61,63,64,63,64,66,62,62,62,69,66,67,63,62,63,61,65,70,79,78,78,76,74,72],
            "stan": "Zachmurzenie"
        },
        "wwa": {
            "img": "img/maps/s_g/warszawa.png",
            "temp": [6,5,5,5,5,5,5,4,5,6,8,10,12,13,13,13,12,10,9,9,9,9,8,8],
            "wl": [62,63,64,63,64,65,62,62,63,69,66,67,65,64,66,66,70,74,76,77,79,78,76,75],
            "stan": "Zachmurzenie"
        }
    }
}

const citysNames = {
    //nazwa z dużej litery i polskimi znakami
    "bialystok": "Białystok",
    "bydgoszcz": "Bydgoszcz",
    "gdansk": "Gdańsk",
    "gorzow": "Gorzów Wielkopolski",
    "katowice": "Katowice",
    "krakow": "Kraków",
    "lodz": "Łódź",
    "lublin": "Lublin",
    "olsztyn": "Olsztyn",
    "poznan": "Poznań",
    "rzeszow": "Rzeszów", 
    "suwalki": "Suwałki",
    "szczecin": "Szczecin",
    "warszawa": "Warszawa",
    "wroclaw": "Wrocław",
    "zielona": "Zielona Góra"
}

const citys = ["bialystok", "bydgoszcz", "gdansk", "gorzow", "katowice", "krakow", "lodz", "lublin", "olsztyn", "poznan", "rzeszow", "suwalki", "szczecin", "warszawa", "wroclaw", "zielona"]
const days_oftheweek = ["niedz", "pon", "wt", "sro", "czw", "pt", "sob"]
const style = {
    // wysokość = Wysokosć orginalna + 1
    // top zawsze jest ujemne

    "bialystok": [
        97, //szerokość
        25, //wysokość
        -314.60, //top
        475 //left
    ],
    "bydgoszcz": [
        105, //szerokość
        25, //wysokość
        -390.7, //top
        210 //left
    ],
    "gdansk": [
        130, //szerokość
        25, //wysokość
        -480.5, //top
        254 //left
    ],
    "gorzow": [
        127, //szerokość
        24, //wysokość
        -391.75, //top
        75 //left
    ],
    "katowice": [
        96, //szerokość
        26, //wysokość
        -218.5, //top
        252 //left
    ],
    "krakow": [
        96, //szerokość
        24, //wysokość
        -222.7, //top
        362 //left
    ],
    "lodz": [
        97, //szerokość
        25, //wysokość
        -342.7, //top
        307 //left
    ],
    "lublin": [
        95, //szerokość
        23, //wysokość
        -365, //top
        468 //left
    ],
    "olsztyn": [
        98, //szerokość
        26, //wysokość
        -555, //top
        348 //left
    ],
    "poznan": [
        96, //szerokość
        23, //wysokość
        -508.75, //top
        236 //left
    ],
    "rzeszow": [
        97, //szerokość
        26, //wysokość
        -336, //top
        465 //left
    ],
    "suwalki": [
        100, //szerokość
        25, //wysokość
        -667.5, //top
        449 //left
    ],
    "szczecin": [
        100, //szerokość
        26, //wysokość
        -686, //top
        82 //left
    ],
    "warszawa": [
        100, //szerokość
        24, //wysokość
        -584.7, //top
        371 //left
    ],
    "wroclaw": [
        97, //szerokość
        28, //wysokość
        -524.9, //top
        162 //left
    ],
    "zielona": [
        128, //szerokość
        27, //wysokość
        -621, //top
        60 //left
    ],
}

let dane = {
    "bialystok": {
        "wojewodztwo": "Podlaskie",
        "populacja": "297 554 mieszkańców",
        "lokacje": "Pałac Branickich. 491 recenzji. Bazylika Archikatedralna Wniebowzięcia Najświętszej Maryi Panny. 110 recenzji. Rynek Kościuszki. 284 recenzje. Akcent ZOO. 85 recenzji. Opera i Filharmonia Podlaska."
    },
    "bydgoszcz": {
        "wojewodztwo": "Kujawsko-Pomorskie",
        "populacja": "354 006 mieszkańców",
        "lokacje": 'Wyspa Młyńska, Wyspa Młyńska, Spichrze nad Brdą, Łuczniczka,"Przechodzący przez rzekę"'
    },
    "gdansk": {
        "wojewodztwo": "Pomorskie",
        "populacja": "470 907  mieszkańców",
        "lokacje": "Muzeum Bursztynu, Dom Uphagena, Fontanna Neptuna w Gdańsku, Ratusz Głównego Miasta, Droga Królewska w Gdańsku"
    },
    "gorzow": {
        "wojewodztwo": "Lubuskie",
        "populacja": "123 609 mieszkańców",
        "lokacje": "Bulwar Nadwarciański, Park Wiosny Ludów, Park Siemiradzkiego, Katedra Wniebowzięcia Najświętszej Maryi Panny, Filharmonia Gorzowska"
    },
    "katowice": {
        "wojewodztwo": "Śląskie",
        "populacja": "292 774 mieszkańców",
        "lokacje": "Kopalnia GUIDO, Muzeum Śląskie, Dolina Trzech Stawów, Nikiszowiec, Park Śląski"
    },
    "krakow": {
        "wojewodztwo": "Małopolskie",
        "populacja": "780 981 mieszkańców",
        "lokacje": "Wawel, Kościół Mariacki, Auschwitz Birkenau, Kopalnia Soli w Wieliczce, Uniwersytet Jagielloński"
    },
    "lodz": {
        "wojewodztwo": "Łódzkie",
        "populacja": "677 286 mieszkańców",
        "lokacje": "Ulica Piotrowska, Pałac Izraela Poznańskiego, Muszeum Kinematografii, Centralne Muzeum Włókiennictwa, Łódzki Ogród Botaniczny"
    },
    "lublin": {
        "wojewodztwo": "Lubelskie",
        "populacja": "339 784 mieszkańców",
        "lokacje": "Archikatedra Lubelska, Baszta Zamkowa - Donżon, Dawna Uczelnia Mędrców Lublina, Kaplica Trójcy Świętej, Lubelska Trasa Podziemna"
    },
    "olsztyn": {
        "wojewodztwo": "Warmińsko-Mazurskie",
        "populacja": "171 979 miesznańców",
        "lokacje": "Park im. Janusza Kusocińskiego, Plac Solidarności, Planetarium, Hala Urania, Stare Miasto"
    },
    "poznan": {
        "wojewodztwo": "Wielkopolskie",
        "populacja": "534 813 mieszkańców",
        "lokacje": "Poznański Stary Rynek - Ratusz, domy budników, pałac Mielżyńskich, Muzeum Powstania Wielkopolskiego, Poznańska Fara, Biblioteka Raczyńskich, Uniwersytet Adama Mickiewicza"
    },
    "rzeszow": {
        "wojewodztwo": "Podkarpackie",
        "populacja": "196 821 mieszkańców",
        "lokacje": "Dworzec kolejowy w Rzeszowie, Plac Cichociemnych, Plac Ofiar Getta, Stary Cmentarz, Fontanna Multimedialna"
    },
    "suwalki": {
        "wojewodztwo": "Podlaskie",
        "populacja": "69 758 mieszkańców",
        "lokacje": "Park Konstytucji 3 Maja, Muzeum Marii Konopnickiej, Plac Marii Konopnickiej, Archiwum Państwowe, Konkatedra św. Aleksandra"
    },
    "szczecin": {
        "wojewodztwo": "Zachodniopomorskie",
        "populacja": "401 907 mieszkańców",
        "lokacje": "Muzeum Techniki i Komunikacji, Krzywy Las, Puszcza Bukowa, Nowe Warpno, Świnoujście"
    },
    "warszawa": {
        "wojewodztwo": "Mazowieckie",
        "populacja": "1 790 658 mieszkańców",
        "lokacje": "Pałac Kultury, Stare miasto, Plac Zamkowy, Łazienki Królewskie, Zamek Królewski"
    },
    "wroclaw": {
        "wojewodztwo": "Dolnośląskie",
        "populacja": "643 782 mieszkańców",
        "lokacje": "Ostrów Tumsk, Wrocławskie krasnale, Rynek we Wrocławiu, Muzeum „Pana Tadeusza”, Uniwersytet Wrocławski"
    },
    "zielona": {
        "wojewodztwo": "Lubuskie",
        "populacja": "141 280 miszkańców",
        "lokacje": "Stary Rynek, konkatedra Św. Jadwigi, Wieża Łaziebna, Muzeum Etnograficzne, Park Winny"
    }
}