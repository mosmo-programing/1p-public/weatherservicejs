// For Hours and Days
now = new Date()
let h = document.querySelector(".godzina")
let d = document.querySelector(".dni")
let godz = ""

// Functions
load_days = () => {
    now_d = new Date()
    if (now_d.getDay() == 0) d.innerHTML = "Niedziela"
    if (now_d.getDay() == 1) d.innerHTML = "Poniedziałek"
    if (now_d.getDay() == 2) d.innerHTML = "Wtorek"
    if (now_d.getDay() == 3) d.innerHTML = "Środa"
    if (now_d.getDay() == 4) d.innerHTML = "Czwartek"
    if (now_d.getDay() == 5) d.innerHTML = "Piątek"
    if (now_d.getDay() == 6) d.innerHTML = "Sobota"
}
load_hour = () => {
    now_h = new Date()
    godz += now_h.getHours()
    godz += ":"
    
    let minutes = now_h.getMinutes()
    if (minutes < 10){
        minutes = "0" + minutes 
    }
    godz += minutes
    godz += ":"

    let seconds = now_h.getSeconds()
    if (seconds < 10){
        seconds = "0" + seconds
    }
    godz += seconds
    h.innerHTML = godz
    godz = ""
}