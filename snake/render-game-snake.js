//zmienne
var wysokoscMapy = 18; 
var szerokoscMapy = 25; 

var snakeX = 2;
var snakeY = 2; 

var wsplXOwoca = 0;  // numer wiersza tabeli
var wsplYOwoca = 0;	 // numer kolumny tabeli

var dlugoscWeza = 0;  

var running = false; 
var gameOver = false;
var wynik = 0;

var kierunek = -1; 
var nowyKierunek = kierunek; 

var idSetInterval;
var postojWeza = 200; 

var lokalizacjaOgonaX = [snakeX]; 
var lokalizacjaOgonaY = [snakeY]; 

function StworzMape()
{
	document.write("<table>");	
	//tworzymy wiersze
	for(var y = 0; y < wysokoscMapy; y++)
	{
		document.write("<tr>");
		
		// tworzymy kolumny w kaĹźdym wierszu
		for(var x = 0; x < szerokoscMapy; x++)
		{
			if(x == 0 || x == szerokoscMapy - 1 || y == 0 || y == wysokoscMapy - 1)
			{
				document.write("<td class='wall' id='" + x + "-" + y + "'></td>");
			}
			else
			{
				document.write("<td class='blank' id='" + x + "-" + y + "'></td>");		
			}
		}
		
		document.write("</tr>");
	}
	document.write("</table>");
	document.write("</div>");
}

function StworzWaz()
{ 
	Set(snakeX, snakeY, "snake");
}

// zmienia klase komorki td 
function Set(x, y, value)
{
	if(x != null && y != null)
	{
		Get(x, y).setAttribute("class", value);
	}
}
// wyciaga nam komorke o zadanym ID
function Get(x, y)
{
	return document.getElementById(x + "-" + y); // <td class="snake" id="2-2" 
}

function StworzObiektyGry()
{
	StworzMape();
	StworzWaz();
	StworzOwoc();
}

// funkcja losujÄca pozycje weza
function Losuj(min, max)
{
	return Math.floor(Math.random() * (max - min) + min); 
}

function OdczytajTypKomorki(wsplX, wsplY)
{
	return Get(wsplX, wsplY).getAttribute("class");  // snake, black, wall, 
}

function StworzOwoc()
{
	var found = false;  // czy znaleĹşliĹmy miejsce na nasz nowy owoc
	
	// "!found" to jest to samo co "found == false"
	while(!found && (dlugoscWeza < (szerokoscMapy - 2) * (wysokoscMapy - 2) + 1))
	{		
		var randXOwoca = Losuj(1, szerokoscMapy - 1); 
		var randYOwoca = Losuj(1, wysokoscMapy - 1); 
		
		if(OdczytajTypKomorki(randXOwoca, randYOwoca) == "blank")
		{
			found = true;
			
			// komorce zmieniamy wartosc class na fruit
			Set(randXOwoca, randYOwoca, "fruit"); 
			
			wsplXOwoca = randXOwoca;
			wsplYOwoca = randYOwoca;
		}	
	}
}

window.addEventListener("keypress", function PobierzKodPrzycisku(event)
{
	var kodPrzycisku = event.keyCode; // wyciÄgamy kod uĹźytego przycisku
	
	
	if(kierunek != -1 && (kodPrzycisku == 119 || kodPrzycisku == 87))
	{
		nowyKierunek = 0;
	}	
	else if(kierunek != 0 && (kodPrzycisku == 115 || kodPrzycisku == 83))
	{
		nowyKierunek = -1;
	}
	else if(kierunek != 2 && (kodPrzycisku == 97 || kodPrzycisku == 65))
	{
		nowyKierunek = 1;
	}
	else if(kierunek != 1 && (kodPrzycisku == 100 || kodPrzycisku == 68))
	{
		nowyKierunek = 2;
	}
	
	// jeĹźeli gra sie nie rozpoczeĹa (!running to false) to uruchom gre
	if(!running)
	{
		running = true;
	}
	else if(kodPrzycisku == 32)
	{
		running = false;
	}
	
}
);
function WykonujAkcjeGry()
{
	if(running && !gameOver)
	{
		AktualizujStanGry();
	}
	else if(gameOver)
	{
		clearInterval(idSetInterval); 
	}
}

function AktualizujStanGry()
{
	kierunek = nowyKierunek;
	
	AktulizujOgon(); 
	
	if(kierunek == 0)
	{
		snakeY--;
	}
	else if(kierunek == -1)
	{
		snakeY++;
	}
	else if(kierunek == 1)
	{
		snakeX--;
	}
	else if(kierunek == 2)
	{
		snakeX++;		
	}
	
	Set(snakeX, snakeY, "snake");
	
	
	for(var i = lokalizacjaOgonaX.length - 1; i >= 0; i--)
	{
		if(snakeX == lokalizacjaOgonaX[i] && snakeY == lokalizacjaOgonaY[i])
		{
			gameOver = true;
			break; 
		}
	}
	
	if(snakeX == 0 || snakeX == szerokoscMapy - 1 || snakeY == 0 || snakeY == wysokoscMapy - 1)
	{
		gameOver = true;
	}
	else if(snakeX == wsplXOwoca && snakeY == wsplYOwoca)
	{
		wynik++;
		
		StworzOwoc();
		
		dlugoscWeza++;
	}
	
}


function AktulizujOgon()
{
	for(var i = dlugoscWeza; i > 0; i--)
	{
		lokalizacjaOgonaX[i] = lokalizacjaOgonaX[i-1];
		lokalizacjaOgonaY[i] = lokalizacjaOgonaY[i-1];
	}
	
	lokalizacjaOgonaX[0] = snakeX;
	lokalizacjaOgonaY[0] = snakeY;
	
	Set(lokalizacjaOgonaX[dlugoscWeza], lokalizacjaOgonaY[dlugoscWeza], "blank");
	
}




function Uruchom()
{
	
	StworzObiektyGry();
	
	idSetInterval = setInterval(WykonujAkcjeGry, postojWeza); 
}


Uruchom();

